class Surface {

  constructor(
    private _x: number = 0,
    private _y: number = 0,
    private _width: number = 0,
    private _height: number = 0, 
  ) {}
  get x() {
    return this._x
  }

  get x2() {
    return this._x + this._width
  }

  get y() {
    return this._y
  }

  get y2() {
    return this._y + this._height
  }

  get width() {
    return this._width
  }

  get height(){
    return this._height
  }

  set x(value: number) {
    this._x = value
  }

  set y(value: number) {
    this._y = value
  }

  set width(value: number) {
    this._width = value
  }

  set height(value: number) {
    this._height = value
  }
}

export default Surface
