import { Geometry } from '../Geometry'
import { Mesh } from '../Mesh'
import { GeometricalRepresentationInterface } from '../../utils/Interfaces' 

class Player extends Geometry implements GeometricalRepresentationInterface {
  constructor(x: number, y: number, color: string) {
    super(x, y)
    this._mesh = new Mesh(x, y, 0, 0, [
      new Geometry(x + 10, y + 0, 0, 0, 10, 10, color),
      new Geometry(x + 0, y + 10, 0, 0, 10, 10, color),
      new Geometry(x + 10, y + 10, 0, 0, 10, 10, color),
      new Geometry(x + 20, y + 10, 0, 0, 10, 10, color),
    ])
    let biggestX: number = -Infinity, smallestX: number = Infinity, biggestY: number = -Infinity, smallestY: number = Infinity
    this._mesh.fragments.forEach((fragment: Geometry) => {
      if (fragment.x2 > biggestX) biggestX = fragment.x2
      if (fragment.x < smallestX) smallestX = fragment.x
      if (fragment.y2 > biggestY) biggestY = fragment.y2
      if (fragment.y < smallestY) smallestY = fragment.y
    })
    this.width = biggestX - smallestX
    this.height = biggestY - smallestY
    window.addEventListener('keydown', this.handleKeydown)
    window.addEventListener('keyup', this.handleKeyup)
  }
  private _mesh: Mesh

  get mesh(){
    return this._mesh
  }
  set mesh(value: Mesh) {
    this._mesh = value
  }

  private handleKeydown = (evt: any): void => {
    const { keyCode } = evt
    switch(keyCode) {
      case 32:
        this.shoot()
        break
      case 37:
        this.vx = -2
        break
      case 38:
        this.vy = -2
        break
      case 39:
        this.vx = 2
        break
      case 40:
        this.vy = 2
        break
      default:
    }
  }

  private handleKeyup = (evt: any): void => {
    const { keyCode } = evt
    if (keyCode === 37 || keyCode === 39) this.vx = 0
    if (keyCode === 38 || keyCode === 40) this.vy = 0
  }

  public onUpdate = (): void => {
    this.mesh.vx = this.vx
    this.mesh.vy = this.vy
    this.mesh.update()
  }

  public draw = (ctx: any): void => {
    this.mesh.draw(ctx)
  }

  public removeEvents = (): any => {
    document.removeEventListener('keydown', this.handleKeydown)
    document.removeEventListener('keyup', this.handleKeyup)
  }

  public prepareDestruction = (): any => {
    this.removeEvents()
    return this
  }

  public shoot = (): any => {}

  public getGeometricalRepresentation = (): any | any[] => {
    return this._mesh.getGeometricalRepresentation()
  }
}

export default Player
