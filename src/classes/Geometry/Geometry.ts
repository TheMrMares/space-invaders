import { Surface } from '../Surface'
import { GeometricalRepresentationInterface, CollisionIndicatorsInterface, CollisionEffectsInterface, IndexSignatureInterface } from '../../utils/Interfaces' 

interface OldGeometricalValuesInterface {
  x: number,
  y: number,
}

class Geometry
extends Surface
implements GeometricalRepresentationInterface, CollisionIndicatorsInterface, IndexSignatureInterface {
  constructor(
    x: number = 0,
    y: number = 0,
    private _vx: number = 0,
    private _vy: number = 0,
    width: number = 0,
    height: number = 0, 
    private _color: string = '#ff0000',
    private _health: number = 1,
  ) {
    super(x, y, width, height)
  }

  [key: string]: any

  private _old: OldGeometricalValuesInterface[] = []

  public draw(ctx: any, xModificator: number = 0, yModificator: number = 0, colorModificator?: string): void {
    ctx.fillStyle = colorModificator || this.color
    ctx.fillRect(this.x + xModificator, this.y + yModificator, this.width, this.height)
  }

  public update = (): void => {
    this.saveOldGeometricalValues()
    this.x += this.vx
    this.y += this.vy
    this.onUpdate()
  }

  public onUpdate = (): void => {}
  
  // improve to work properly with receiveCollision for player
  private saveOldGeometricalValues = (): void | any => {
    const lastSaved = this.old[this.old.length - 1]
    const emptyArrayCondition = this.old.length === 0
    const diffrentLastSavedCondition = lastSaved && (lastSaved.x !== this.x || lastSaved.y !== this.y)
    if (emptyArrayCondition || diffrentLastSavedCondition) {
      this.old.push({
        x: this.x,
        y: this.y,
      })
    }
    
    if (this.old.length > 5) this.old.shift()
  }

  public getGeometricalRepresentation = (): any | any[] => {
    return this
  }

  public provideCollision = (hitObject: any): void => {
    hitObject.receiveCollision()
  }

  public receiveCollision = (): void => {
    const lastSaved = this.old[this.old.length - 1]
    this.vx = 0
    this.vy = 0
    if (lastSaved) {
      this.x = lastSaved.x
      this.y = lastSaved.y
    }
  }

  public prepareDestruction = (): any => this

  get old() {
    return this._old
  }

  get vx() {
    return this._vx
  }

  get vy(){
    return this._vy
  }

  get color(){
    return this._color
  }

  get health() {
    return this._health
  }

  set old(value: OldGeometricalValuesInterface[]) {
    this._oldx = value
  }

  set vx(value: number) {
    this._vx = value
  }

  set vy(value: number) {
    this._vy = value
  }

  set color(value: string) {
    this._color = value
  }

  set health(value: number) {
    this._health = value
  }
}

export default Geometry
