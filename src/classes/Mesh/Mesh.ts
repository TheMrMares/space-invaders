import { Geometry } from '../Geometry'
import { GeometricalRepresentationInterface } from '../../utils/Interfaces' 

class Mesh extends Geometry implements GeometricalRepresentationInterface {
  constructor(
    x: number,
    y: number,
    vx: number,
    vy: number,
    private _fragments: Geometry[]) {
      super(x, y, vx, vy)
  }

  get fragments(){
    return this._fragments
  }
  set fragments(value: Geometry[]) {
    this._fragments = value
  }

  public onUpdate = (): void => {
    this.fragments.forEach((fragment: Geometry) => {
      fragment.vx = this.vx
      fragment.vy = this.vy
      fragment.update()
    })
  }

  public draw = (ctx: any): void => {
    this.fragments.map((fragment: Geometry) => fragment.draw(ctx))
  }

  public getGeometricalRepresentation = (): any | any[] => {
    return this.fragments
  }
}

export default Mesh
