import { Geometry } from '../Geometry'
import { CollisionEffectsInterface } from '../../utils/Interfaces'

class Bullet extends Geometry {
  constructor(...args: any) {
    super(...args)
  }

  public provideCollision = (hitObject: any): void => {
    hitObject.receiveCollision()
    hitObject.health -= 5
  }

  public receiveCollision = (): void => {
    this.health = -Infinity
  }
}

export default Bullet
