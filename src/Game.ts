import { Player } from './classes/Player'
import { Geometry } from './classes/Geometry'
import { Bullet } from './classes/Bullet'
import { Collisions, Arrays } from './utils/Utils'

class Canvas {
  area: any = undefined
  width: number = 0
  height: number = 0
  ctx: any = undefined
}

class Loop {
  lastFrame: number = 0
  delta: number = 0
  timestep: number = 0
  numberOfUpdates: number = 0
  started: boolean = false
}

class Game {
  constructor(canvas: any) {
    this.canvas = {
      area: canvas,
      width: canvas.offsetWidth,
      height: canvas.offsetHeight,
      ctx: canvas.getContext('2d'),
    }
    this.player = new Player(375, 580, '#00ff00'),
    this.livingObjects = [
      this.player,
      new Geometry(340, 500, 0, 0, 10, 10, '#ff0000', 40),
      new Geometry(370, 500, 0, 0, 10, 10, '#ff0000', 20),
    ]
    this.player.shoot = (): void => {
      this.livingObjects.push(
        new Bullet(
          this.player.x + this.player.width/2,
          this.player.y - 20,
          0, -6,
          2, 20,
          '#ff00ff')
      )
    }
  }
  private player: Player
  private canvas: Canvas
  private loop: Loop = {
    lastFrame: 0,
    delta: 0,
    timestep: 1000/60,
    numberOfUpdates: 240,
    started: false,
  }
  private livingObjects: any[]

  private update = (): void => {
    this.livingObjects.forEach((obj: any) => obj.update())
    Collisions.getCollisionsArray(this.livingObjects, (firstObj: Geometry, secondObj: Geometry) => {
      if (secondObj.provideCollision) secondObj.provideCollision(firstObj)
      if (firstObj.provideCollision) firstObj.provideCollision(secondObj)
    })
    this.livingObjects = this.livingObjects.filter((obj: any) => {
      if (obj.health > 0) return true
      if (obj.prepareDestruction) obj.prepareDestruction()
      return false
    })
  }

  private draw = (): void => {
    const { canvas, livingObjects } = this
    const { ctx, width, height } = canvas
    ctx.fillStyle = '#000000'
    ctx.fillRect(0, 0, width, height)
    livingObjects.forEach(obj => obj.draw(ctx))
  }

  private tick = (timestamp: number): void => {
    const { loop } = this
    loop.delta += timestamp - loop.lastFrame
    loop.lastFrame = timestamp
    while (loop.delta >= loop.timestep) {
      this.update()
      loop.delta -= loop.timestep
      if (loop.numberOfUpdates >= 240) {
        loop.delta = 0;
        break;
      }
    }
    this.draw()
    requestAnimationFrame(this.tick)
  }

  public start = (): void => {
    const { loop } = this
    if (!loop.started) {
      loop.started = true
      requestAnimationFrame(this.tick)
    }
    
  }
}

export default Game
