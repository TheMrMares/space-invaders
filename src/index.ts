import Game from './Game'

const root = document.querySelector('#root');
const game = new Game(root);
game.start()
