export interface IndexSignatureInterface {
  [key: string]: any
}

export interface GeometricalRepresentationInterface {
  getGeometricalRepresentation: () => any | any[]
}

export interface CollisionIndicatorsInterface {
  provideCollision?: (...args: any) => any
  receiveCollision?: (...args: any) => void
}

export  interface CollisionEffectsInterface {
  damage?: number,
  heal?: number,
}
