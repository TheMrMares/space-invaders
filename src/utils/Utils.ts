import { Geometry } from '../classes/Geometry'
import { Surface } from '../classes/Surface'

const asArray = (arrayOrObject: any): any[] => {
  if (typeof arrayOrObject === 'object' && arrayOrObject.length !== undefined) {
    return arrayOrObject
  }
  return [arrayOrObject]
} 

const eraseArrayElement = (array: any[], index: number) => {
  return array.filter((e: any, i: number) => i !== index)
}

const checkCollisionBetween = (arrayOrObjA: Geometry | Geometry[], arrayOrObjB: Geometry | Geometry[], callback?: Function): boolean => {
  const arrayA: Geometry[] = asArray(arrayOrObjA)
  const arrayB: Geometry[] = asArray(arrayOrObjB)
  for(let firstIndex: number = 0; firstIndex < arrayA.length; firstIndex++ ) {
    for(let secondIndex: number = 0; secondIndex < arrayB.length; secondIndex++ ) {
      if (checkCollision(arrayA[firstIndex], arrayB[secondIndex])) {
        if(callback) {
          callback(arrayA[firstIndex], arrayB[secondIndex])
        }
        return true
      }
    }
  }
  return false
}

const getCollisionsArray = (arr: Geometry[], callback: Function): Geometry[] => {
  const tmpArray: Geometry[] = []
  for(let firstIndex: number = 0; firstIndex < arr.length - 1; firstIndex++ ) {
    let alreadySavedFlag = false
    for(let secondIndex: number = firstIndex + 1; secondIndex < arr.length; secondIndex++ ) {
      if (checkCollisionBetween(arr[firstIndex].getGeometricalRepresentation(), arr[secondIndex].getGeometricalRepresentation())) {
        if (!alreadySavedFlag) tmpArray.push(arr[firstIndex])
        alreadySavedFlag = true
        tmpArray.push(arr[secondIndex])
        callback(arr[firstIndex], arr[secondIndex])
      }
    }
  }
  return tmpArray
}

const checkAxisCollision = (objA: Geometry, objB: Geometry, axis: string): boolean => {
  const objectAAxisBetweenObjectBAxis = objA[axis] >= objB[axis] && objA[axis] <= objB[`${axis}2`]
  const objectAAxis2BetweenObjectBAxis = objA[`${axis}2`] >= objB[axis] && objA[`${axis}2`] <= objB[`${axis}2`]
  const objectBAxisBetweenObjectAAxis = objB[axis] >= objA[axis] && objB[axis] <= objA[`${axis}2`]
  const objectBAxis2BetweenObjectAAxis = objB[`${axis}2`] >= objA[axis] && objB[`${axis}2`] <= objA[`${axis}2`]
  const XAxisCondition = objectAAxisBetweenObjectBAxis || objectAAxis2BetweenObjectBAxis || objectBAxisBetweenObjectAAxis || objectBAxis2BetweenObjectAAxis
  return XAxisCondition
}

const checkCollision = (objA: Geometry, objB: Geometry, callback?: Function): boolean => {
  if (checkAxisCollision(objA, objB, 'x') && checkAxisCollision(objA, objB, 'y')) {
    if (callback) callback()
    return true
  }
  return false
}

interface isInAreaModificatorInterface {
  top: number,
  bottom: number,
  left: number,
  right: number,
}

const isInArea = (object: Geometry, area: Surface, modificator: isInAreaModificatorInterface = { top: 0, bottom: 0, left: 0, right: 0 }): boolean => {
  const { top, bottom, left, right } = modificator
  const objectXBetweenAreaX = object.x >= area.x + left && object.x2 <= area.x2 + right
  const objectYBetweenAreaY = object.y >= area.y + top && object.y2 <= area.y2 + bottom
  return objectXBetweenAreaX && objectYBetweenAreaY
}

export const Arrays: any = {
  asArray,
  eraseArrayElement,
}

export const Collisions: any = {
  checkAxisCollision,
  checkCollisionBetween,
  getCollisionsArray,
  checkCollision,
  isInArea,
}
